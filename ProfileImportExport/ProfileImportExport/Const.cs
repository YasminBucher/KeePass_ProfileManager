﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfileImportExport
{
	class Const
	{
		private Const()
		{
		}

		public const string PROFILE_FILE_EXT = ".kpp";
	}
}