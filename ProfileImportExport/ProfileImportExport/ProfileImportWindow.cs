﻿using KeePassLib.Cryptography.PasswordGenerator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ProfileImportExport
{
	public partial class ProfileImportWindow : Form
	{
		public PwProfile Profile { get; private set; }

		public ProfileImportWindow()
		{
			InitializeComponent();
		}

		private void btnSelectProfileFile_Click(object sender, EventArgs e)
		{
			using(OpenFileDialog ofd = new OpenFileDialog())
			{
				ofd.Filter = string.Format("KeePass Profil Datei (*{0})|*{0}", Const.PROFILE_FILE_EXT);

				if(ofd.ShowDialog() == DialogResult.OK)
				{
					this.txtProfileFile.Text = ofd.FileName;
				}
			}

			this.LoadProfile();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
		}

		private void btnImport_Click(object sender, EventArgs e)
		{
			this.Profile.Name = this.txtProfileName.Text;
			this.DialogResult = DialogResult.OK;
		}

		private void txtProfileName_TextChanged(object sender, EventArgs e)
		{
			//maybe check if profile name already taken?
		}

		private void LoadProfile()
		{
			PwProfile tmpProfile = null;

			XmlSerializer ser = new XmlSerializer(typeof(PwProfile));
			using(FileStream fs = new FileStream(this.txtProfileFile.Text, FileMode.Open, FileAccess.Read, FileShare.Read))
			{
				using(StreamReader sr = new StreamReader(fs))
				{
					tmpProfile = (PwProfile)ser.Deserialize(sr);
				}
			}

			if(tmpProfile != null)
			{
				this.Profile = tmpProfile;
				this.txtProfileName.Text = this.Profile.Name;
			}
		}
	}
}