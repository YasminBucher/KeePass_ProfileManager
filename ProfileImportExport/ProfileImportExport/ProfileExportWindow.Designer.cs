﻿namespace ProfileImportExport
{
	partial class ProfileExportWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbProfileSelection = new System.Windows.Forms.ComboBox();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnExport = new System.Windows.Forms.Button();
			this.lblProfileSelection = new System.Windows.Forms.Label();
			this.lblProfileSaveFile = new System.Windows.Forms.Label();
			this.txtProfileSaveFile = new System.Windows.Forms.TextBox();
			this.btnSelectProfileSaveFile = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// cmbProfileSelection
			// 
			this.cmbProfileSelection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.cmbProfileSelection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbProfileSelection.FormattingEnabled = true;
			this.cmbProfileSelection.Location = new System.Drawing.Point(12, 25);
			this.cmbProfileSelection.Name = "cmbProfileSelection";
			this.cmbProfileSelection.Size = new System.Drawing.Size(430, 21);
			this.cmbProfileSelection.TabIndex = 0;
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.Location = new System.Drawing.Point(286, 117);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 1;
			this.btnCancel.Text = "Abbrechen";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnExport
			// 
			this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnExport.Location = new System.Drawing.Point(367, 117);
			this.btnExport.Name = "btnExport";
			this.btnExport.Size = new System.Drawing.Size(75, 23);
			this.btnExport.TabIndex = 2;
			this.btnExport.Text = "Exportieren";
			this.btnExport.UseVisualStyleBackColor = true;
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			// 
			// lblProfileSelection
			// 
			this.lblProfileSelection.AutoSize = true;
			this.lblProfileSelection.Location = new System.Drawing.Point(12, 9);
			this.lblProfileSelection.Name = "lblProfileSelection";
			this.lblProfileSelection.Size = new System.Drawing.Size(30, 13);
			this.lblProfileSelection.TabIndex = 3;
			this.lblProfileSelection.Text = "Profil";
			// 
			// lblProfileSaveFile
			// 
			this.lblProfileSaveFile.AutoSize = true;
			this.lblProfileSaveFile.Location = new System.Drawing.Point(12, 69);
			this.lblProfileSaveFile.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
			this.lblProfileSaveFile.Name = "lblProfileSaveFile";
			this.lblProfileSaveFile.Size = new System.Drawing.Size(98, 13);
			this.lblProfileSaveFile.TabIndex = 4;
			this.lblProfileSaveFile.Text = "Profil-Speicherdatei";
			// 
			// txtProfileSaveFile
			// 
			this.txtProfileSaveFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtProfileSaveFile.Location = new System.Drawing.Point(12, 85);
			this.txtProfileSaveFile.Name = "txtProfileSaveFile";
			this.txtProfileSaveFile.ReadOnly = true;
			this.txtProfileSaveFile.Size = new System.Drawing.Size(398, 20);
			this.txtProfileSaveFile.TabIndex = 5;
			// 
			// btnSelectProfileSaveFile
			// 
			this.btnSelectProfileSaveFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSelectProfileSaveFile.Location = new System.Drawing.Point(416, 83);
			this.btnSelectProfileSaveFile.Name = "btnSelectProfileSaveFile";
			this.btnSelectProfileSaveFile.Size = new System.Drawing.Size(26, 23);
			this.btnSelectProfileSaveFile.TabIndex = 6;
			this.btnSelectProfileSaveFile.Text = "...";
			this.btnSelectProfileSaveFile.UseVisualStyleBackColor = true;
			this.btnSelectProfileSaveFile.Click += new System.EventHandler(this.btnSelectProfileSaveFile_Click);
			// 
			// ProfileExportWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(454, 152);
			this.Controls.Add(this.btnSelectProfileSaveFile);
			this.Controls.Add(this.txtProfileSaveFile);
			this.Controls.Add(this.lblProfileSaveFile);
			this.Controls.Add(this.lblProfileSelection);
			this.Controls.Add(this.btnExport);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.cmbProfileSelection);
			this.Name = "ProfileExportWindow";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Profile Export";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox cmbProfileSelection;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnExport;
		private System.Windows.Forms.Label lblProfileSelection;
		private System.Windows.Forms.Label lblProfileSaveFile;
		private System.Windows.Forms.TextBox txtProfileSaveFile;
		private System.Windows.Forms.Button btnSelectProfileSaveFile;
	}
}