﻿namespace ProfileManager
{
	class Const
	{
		private Const()
		{
		}

		public const string PROFILE_FILE_EXT = ".kpp";
	}
}